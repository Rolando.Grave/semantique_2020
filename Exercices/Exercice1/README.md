# Cours de sémantique

## Premier pas en programmation logique

### Introduction

Durant cette première session, nous allons mettre en place les différents outils qui vous seront utile pendant le semestre.

Vous aurez deux choix possible pour faire de la programmation logique:
- **Prolog**
- **Swift** à l'aide de la librairie **LogicKit**

À vous de tester pour vous donner votre opinion sur ce qui vous plaît le plus.
Pour la première séance, vous aurez un code très similaire en terme de fonctionnalité dans les deux langages. Vous pourrez ainsi comprendre les bases pour vous débrouiller par la suite !
Il est **VIVEMENT** recommandé de bien vous concentrer sur le langage que vous utiliserez.
En particulier pour Swift, où j'ai pris le temps de vous faire des exemples pour vous montrer la plupart des cas de figure.

Si vous avez des questions pour ces deux langages, je suis à votre disposition, surtout si celles-ci ont un lien avec la compréhension.
La programmation logique étant un paradigme nouveau pour vous, le fonctionnement est différent de ce que vous aurez eu l'occasion de voir jusqu'à maintenant.

Raison de plus pour bien comprendre les exemples déjà donnés pour en saisir le sens, pour pouvoir ensuite écrire votre propre code.

### Première partie: Installation

La **première étape** est de lire en complet le README à la racine et de suivre toutes les instructions d'installations qui vous sont données.

**ATTENTION**: Veillez à ce que votre dépôt soit privé !

### Seconde partie: Testez les exemples

Vous avez un dossier pour Prolog et un pour Swift.
En fonction du langage choisie, accédez au dossier correspondant.

Le but de l'exemple est de créer son propre type entier et son propre type liste.
Jouez avec les exemples pour voir ce que vous obtenez.

Dans la programmation logique, le but va être de trouver toutes les valeurs qui sont valides pour des faits et règles donnés.
C'est-à-dire que si vous avez une règle addition, vous pouvez obtenir le résultat, ou vous pouvez trouver toutes les combinaisons qui donnent un résultat.

Exemple en pseudo-code:
```
% Res -> 4
add(1,3, Res).
% À vous de tester
add(X,Y,4).
```

De base, si vous donnez toutes les valeurs à l'intérieur de vos règles, vous aurez comme résultat `true` ou `false`, en fonction de la réussite ou non de l'exécution.
Si vous tentez de mettre une variable, vous obtiendrez les résultats qui satisfont la règle si cela fonctionne.

**Tips Prolog:** De base Prolog affiche seulement la première solution, si vous voulez en voir d'autres il vous suffit d'appuyer sur la barre espace. Pour debugger votre programme, activer le mode trace avec `trace.`. Pour désactiver: `nodebug.`

**Tips LogicKit:** Pour debug, quand vous faites une requête comme celle-ci:  
`var contains0 = kb.ask(.fact("contains", list0, n(3)))`, il vous suffit d'ajouter un second paramètre comme ceci:  
`var contains0 = kb.ask(.fact("contains", list0, n(3)), logger: DefaultLogger(useFontAttributes: false))`.
Vous verrez de la même manière que Prolog toutes les règles utilisés pour obtenir un résultat.

**ATTENTION**: Avancez pas à pas et vérifiez bien à chaque étape que tout fonctionne. Il devient très vite difficile de debug un programme faisant appel à plusieurs règles. Si vous voulez donc debug un programme, assurez vous toujours de prendre l'exemple le plus minimale possible !

### Dernière partie: Pour les plus téméraires

Ces langages n'ont déjà plus de secret pour vous, vous avez déjà eu le temps de tout installer et de tout comprendre ?

À vous de jouer et d'implémenter les exemples suivants (en utilisant les types donnés et non les builtins types):
- La soustraction entre deux naturels
- La taille d'une liste
- La somme d'une liste
