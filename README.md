# Sémantique @ [University of Geneva](http://www.unige.ch)

Ce dépôt contient toutes les informations relatives au cours de sémantique.
**N'oubliez pas d'activer la cloche sur "surveiller (watch)" sur la page du [cours](https://gitlab.unige.ch/semantique/semantique_2020)**.
Vous serez ainsi tenu au courant dès qu'une modification sera appliquée au dépôt.

## Informations importantes et liens

* Lien Gitlab: [`https://gitlab.unige.ch/semantique/semantique_2020`](https://gitlab.unige.ch/semantique/semantique_2020)
* Les cours ont lieu le mardi de 10h00 à 12h00
* Les exercices ont lieu le mardi de 14h00 à 16h00
* Team: Prof. Didier Buchs, Damien Morard
* Les TPs comptent pour 1/3 de la note.
* Utiliser les **issues** (tickets) de Gitlab pour communiquer ! Cela évitera d'avoir plusieurs fois la même question ! (Les issues sont sur votre barre des tâches tout à gauche)
* Pour toutes informations plus personnelles vous pouvez nous contacter par mail où directement venir à nos bureaux:
    * `didier.buchs@unige.ch` (office 217)
    * `damien.morard@unige.ch` (office 221)
* Pour ce cours vous pouvez utiliser [Prolog](https://fr.wikipedia.org/wiki/Prolog) ou Swift.

## Environnement

Pour ce cours, il vous faudra **obligatoirement** les éléments suivants:
Nous avons pris soin de rendre les choses aussi simple que possible.

* [Gitlab](https://about.gitlab.com/): Une plate-forme pour avoir à disposition le code source, qui sera utilisé pour les exercices et les travaux pratiques. Vous devez vous connecter avec vos identitfiants unige.
* [MacOS Catalina](https://www.apple.com/chfr/macos/catalina/),
  ou [Ubuntu 18.04 LTS 64bits](https://www.ubuntu.com/download/desktop),
  si besoin vous pouvez utiliser une machine virtuelle, comme par exemple [VirtualBox](http://virtualbox.org),
  ou directement en dualboot.
* [Atom](https://atom.io) (ou [CLion](https://www.jetbrains.com/fr-fr/clion/) pour Swift): L'éditeur de texte que nous utiliserons pour écrire le code.
Vous devez:
* [Surveiller(Watch)](https://gitlab.unige.ch/semantique/semantique_2019) sur la page gitlab du cours avec la petite cloche en haut à droite pour être tenu au courant des modifications à propos du cours.

### Étapes à suivre attentivement

* Créer un nouveau projet privé(private repository) grâce au bouton **+** en haut à gauche de la barre de recherche. Cliquez pour créer un nouveau projet.
Vous devez être sur "Blank project", pour le nom de celui-ci **choisissez exactement** `semantique`.  
Laissez en visibilité **privée** et **ne cochez pas** la case pour initialiser le projet avec un README.

* Setup un couple de clé (public/privé) pour votre compte gitlab. C'est pas compliqué et il vous suffit de suivre attentivement les étapes sur ce [lien](https://gitlab.unige.ch/help/ssh/README#generating-a-new-ssh-key-pair).
Pensez bien à aller jusqu'à la section **Adding an SSH key to your GitLab account**. Toute cette étape est primordiale vu que c'est ce qui vous permettra de mettre votre travail en ligne.
Si vous voulez faire des tests pour être sûr de comprendre comment ça fonctionne n'hésitez pas ! Par contre créez un autre dépôt pour l'occasion, sinon vous aurez des soucis de conflit avec les étapes qui suivront !
Si vous faites le test comme indiqué avec la commande `ssh -T git@gitlab.com` et que cela vous retourne une erreur ne vous inquiétez pas, vous aurez l'occasion de tester le fonctionnement juste après.


* [Clonez votre dépôt créer précédemment dans un dossier de votre choix](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) en utilisant **SSH** et non **HTTPS**. Vous aurez sûrement un message par rapport à la clé que vous avez créé avant, vous aurez simplement à confirmer.

  ```sh
  git clone git@gitlab.unige.ch:YOURUSERNAME/semantique.git
  ```

* Nous allons maintenant relier votre dépôt privé avec celui du cours, pour récupérer tout le contenu du dépôt dans votre dépôt ! Ces opérations s'appliquent **dans** le dossier clôné !

  ```sh
  git remote add course git@gitlab.unige.ch:semantique/semantique_2019.git
  ```

* Une fois que la connexion est faite, il vous suffit d'utiliser la commande ci-dessous pour mettre à jour votre dépôt. Tout le contenu du dépôt du cours sera automatiquement téléchargé dans votre dépôt privé. La commande doit être utilisée à chaque fois qu'une modification est faite sur le dépôt principal.

  ```sh
  git pull course master
  ```

* N'oubliez pas de push ce que vous avez récupéré. Vous pourrez ainsi voir les modifications sur votre dépôt privé. Si vous avez besoin de revoir les commandes git, c'est [ici](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git) !

  ```sh
  git push
  ```

* [Ajouter en collaborateur](https://docs.gitlab.com/ee/user/project/members/)
  l'utilisateur: [`@Damien.Morard`](https://gitlab.unige.ch/Damien.Morard) (Damien Morard).
  Sur votre barre tout à gauche: Paramètres -> Membres
  Pensez bien à mettre l'utilisateur avec le rôle `Maintainer`.

* Commenter l'issue #1 avec votre `Prénom + Nom`, `Le lien de votre profil Gitlab`, `Le lien de votre dépôt privé`, `Votre adresse mail universitaire`.
  Votre réponse aura cette forme (avec vos informations à la place des miennes)
  E.g.  
  Damien Morard, @Damien.Morard, [Dépôt de Damien](https://gitlab.unige.ch/Damien.Morard/semantique), damien.morard@unige.ch.


* [Installer Prolog sur vore machine](https://wwu-pi.github.io/tutorials/lectures/lsp/010_install_swi_prolog.html) ou

* [Installer Swift sur votre machine](https://swift.org/getting-started/#installing-swift)
  * Linux: Suivre les instructions données dans le lien.
  * MacOS: Installer XCode, lancez le et acceptez les conditions d'utilisations.
  * [CLion](https://www.jetbrains.com/clion/): un IDE cross plat-form qui peut être utilisé pour Swift.
[Obtenir une licence gratuite pour étudiant](https://www.jetbrains.com/student/) and [download CLion](https://www.jetbrains.com/shop/download/CL/2019200) (choisissez votre distribution).

  * [Activer Swift pour CLion](https://www.jetbrains.com/help/clion/swift.html).

* Si vous souhaitez utiliser Windows vous pouvez, cependant aucune aide ne sera fourni.

* Devenir familier avec Prolog:
  * Tutoriel: http://www.gecif.net/articles/linux/prolog.html
  * Dire à Prolog dans quel dossier on travaille: https://stackoverflow.com/questions/11633181/swi-prolog-change-working-directory-get-current-working-directory/12864455
  * Dire à Prolog quel fichier on lit: `consult("myFile.pl").`  


* Devenir familier avec Swift:
   * Documentation officiel: https://swift.org/getting-started/
   * Tutorial on Swift by Dimitri Racordon (@kyouko-taiga): https://kyouko-taiga.github.io/swift-thoughts/tutorial/

* Documentation de [Logickit](https://github.com/kyouko-taiga/LogicKit/blob/master/Docs/UserManual.md)

   > The supported OS versions are macOS 10.15 (Catalina) and Ubuntu 18.04.
   > You are highly encouraged to use either of those versions.

Les environnements installés contiennent:
* [Git](https://git-scm.com/docs/gittutorial):
  l'outil pour la gestion de votre code;
* [Atom](https://atom.io) ou [CLion](https://www.jetbrains.com/fr-fr/clion/):
  l'éditeur que nous utiliserons.
  Vous voudrez peut-être ajouter le package `language-swift`. (Peut-être aussi `autocomplete-swift`).



## Rules

* Vous **devez** faire vos travaux dans le **dépôt privé** que vous avez créé (après avoir fork toutes les données du cours).
* Si une raison fait que vous avez des problèmes avec la deadline, contactez
l'assistant aussi vite que possible. (Nous ferons en sorte de trouver une solution).
* L'assistant est le seul à avoir accès à votre code source (et personne d'autre).
* À moins que cela soit **formulé clairement**, les exercices sont des travaux personnels. Aucune collaboration, travail en commun ou bien encore partage de vos travaux ne sera toléré.
Vous pouvez cependant discuter de vos approches entre vous.
* Dès le début du cours, vous avez **2 Jokers**. Chaque Joker correspond à un jour bonus supplémentaire pour rendre vos TPs. C'est-à-dire que si vous dépassez la deadline donnée, un joker sera consommé et vous donnera un jour supplémentaire. Un joker est consommé dans son intégralité, aucune division n'est faite. Si vous dépassez la deadline ne serait-ce que d'une seconde un joker sera consommé.
**Une fois tous les Jokers consommés**, le dépassement de la deadline entraînera une note de **0**.


## TPs (Homeworks)
* Tous les travaux peuvent être trouvés dans le répertoir `TPs/`.
* Il y aura pour chaque TP un sous-répertoire à l'intérieur de `TPs/`.
* **Ne modifier en aucun cas** les fichiers à l'intérieur sauf si cela est explicitement mentionné !
* Lisez les consignes de manière complète et attentive avant de débuter un TP.
* Suivez bien les instructions qui vous sont données pour faire vos TPs.
* Pour les TPs avec du code, il ne faut **aucune erreur de compilation ou de warning**. Si un code ne compile pas c'est **0**. Le compilateur de swift vous avertira s'il y a le moindre soucis de compilation ou de warning. (`swift build` pour compiler)
<!-- * For testing, we use [XCTest](https://developer.apple.com/documentation/xctest).
  It is already installed in your environment,
  and can run all the tests within the test folder files using: `swift test`.
  -->

### TPs Deadlines
Vous avez jusqu'à 23:59:59 (*heure local de Genève*) par rapport à la date de jour de rendu pour mettre en ligne votre solution. Passez ce délai un Joker sera utilisé, et si vous n'avez plus de Joker la note sera de **0**.
Si vous rencontrez une situation difficile où vous avez besoin d'une extension, merci de nous prévenir à l'avance ! (À l'avance ne signifie pas deux heures avant la deadline)

| No.  |    1     |    2     |    3     |    4     |    5     |    6    |
| ---- | -------- | -------- | -------- | -------- | -------- | --------|
| Date |     -    |    -     |    -     |     -    |     -    |     -   |

### TPs  Reviews

* En plus des TPs à soumettre, vous serez invité à venir expliquer votre TP de manière occasionnel. Ceci est **obligatoire** et comptera pour une partie de la note des TPs.
* Chaque fois qu'un TP aura été rendu, une sélection aléatoire d'étudiant(e)s sera réalisée pour venir à nos bureux et expliquer leur TP.
* La review durera entre 5 et 10 minutes pendant lesquelles l'assistant pourra vous poser des questions en rapport avec le TP.
* La review sera noté et comptera dans l'évaluation des travaux pratiques.

### Evaluation

* La note finale de vos TPs sera calculée ainsi:
La moyenne de vos TPs reviews comptent pour un TP. Donc si vous avez 6 TPs la moyenne des TPs reviews comptera comme un 7ème TP.
* Voici un exemple de comment le calcul sera effectué, en prenant en compte les reviews.
* Le nombre de TPs et de reviews donné dans l'exemple est à titre indicatif, et sert juste à expliquer le fonctionnement du calcul de la moyenne finale des TPs.


   | TP1 | TP2 | TP3 | TP4 | TP5 | TP6 | R1  | R2  | R3  |
   | --- | --- | --- | --- | --- | --- | --- | --- | --- |
   |  4  |  5  |  4  |  5  |  6  |  5  |  6  |  4  |  5  |

   Moyenne des TPs reviews: AR = (6 + 4 + 5) / 3 = 5  
   Note finale = (TP1 + TP2 + TP3 + TP4 + TP5 + TP6 + AR) / 7  
   = (4 + 5 + 4 + 5 + 6 + 5 + 5) / 7 = 4.85

### Obtenir de l'aide / Getting Help

Vous devez être assez mature et intelligent pour résoudre les problèmes par vous-mêmes.
I.e.:
*Si vous rencontre des problèmes, résolvez les !*  

Dans la situation où vous n'arriveriez pas à résoudre un problème, voici la liste ordonnée des tâches à faire:
  1. Google is your friend. (Temps de réponse < 1 sec.)
  2. Read the manual. (Temps de réponse < 1 min.)
  3. Demander à un ami/collègue. (Temps de réponse < 30 mins.)
  4. Stackoverflow. [Apprenez](https://stackoverflow.com/help/how-to-ask) (Temps de réponse < 12 hrs.)
  5. L'assistant. (Temps de réponse < 1 day.)
  6. Professeur. (Temps de réponse ???)


### Homework #0 (Non noté)

**Deadline 25.02.2020**

**Swift:**

Faites fonctionner votre environnement !
Plus spéficiquement :
* [ ] Avez-vous surveiller le dépôt ?
* [ ] Avez-vous créé votre propre dépôt privé, préparé le nécessaire expliqué ?
* [ ] Avez-vous testé le `git pull course master` pour récupérer les fichiers du cours ?
* [ ] Avez-vous ajouté @damien.morard comme collaborateur ?
* [ ] Avez-vous installé Swift ?
* [ ] Avez-vous vérifié si vous pouvez créer un package swift ? (`mkdir Hello;cd Hello;swift package init`) ?
* [ ] Avez-vous vérifié que vous pouvez compiler un package swift ? (`swift build`)
* [ ] Avez-vous vérifié que vous pouvez utiliser la fonction test de swift ? (`swift test`)
* [ ] Avez-vous installé un éditeur de code ? (i.e Atom)
* [ ] Avez-vous lu le [tutoriel swift](https://kyouko-taiga.github.io/swift-thoughts/tutorial/)?
* [ ] Avez-vous répondu à l'issue [#1](https://gitlab.unige.ch/semantique/semantique_2020/issues/1) avec votre `Prénom + Nom`, `Le lien de votre profil Gitlab`, `Le lien de votre dépôt privé`, `Votre adresse mail universitaire`. (Exemple disponible directement dans l'issue)

**Prolog:**

Faites fonctionner votre environnement !
Plus spéficiquement :
* [ ] Avez-vous surveiller le dépôt ?
* [ ] Avez-vous créé votre propre dépôt privé, préparé le nécessaire expliqué ?
* [ ] Avez-vous testé le `git pull course master` pour récupérer les fichiers du cours ?
* [ ] Avez-vous ajouté @damien.morard comme collaborateur ?
* [ ] Avez-vous installé un éditeur de code ? (i.e Atom)
* [ ] Avez-vous installé Prolog ?
* [ ] Avez-vous testé l'interprète Prolog ?
* [ ] Avez-vous vérifier si vous pouvez import un fichier Prolog ?
* [ ] Avez-vous testé d'utiliser le fichier import ?
* [ ] Avez-vous lu le [tutoriel Prolog](http://www.gecif.net/articles/linux/prolog.html) ?
* [ ] Avez-vous répondu à l'issue [#1](https://gitlab.unige.ch/semantique/semantique_2020/issues/1) avec votre `Prénom + Nom`, `Le lien de votre profil Gitlab`, `Le lien de votre dépôt privé`, `Votre adresse mail universitaire`. (Exemple disponible directement dans l'issue)

<!--
### Homework #1

The source files are located within: `homework/hw1_petrinets/`.
You have to understand the provided code and fill in the missing code (marked with `TODO`).
Do **not** touch the existing code or tests,
but you can add your own tests **in a new file** within the `Tests` folder.

The deadline is 10 october 2017 at 23:59.
We will clone all your repositories using a script,
so make sure that @stklik and @damdamo have read access.

Evaluation will be:

* have you done anything at the deadline?
  (yes: 1 point, no: 0 point)
  * [ ] Done anything
* do you have understood and implemented all the required notions?
  (all: 3 points, none: 0 point)
  * [ ] Reachability graph
  * [ ] Coverability graph
* do you have understood and implemented corners cases of all the required
  notions?
  (all: +2 points, none: 0 point)
  * [ ] Reachability graph
  * [ ] Coverability graph
* do you have correctly written and tested your code?
  (no: -0.5 point for each)
  * [ ] Coding standards
  * [ ] Tests
  * [ ] Code coverage

| Grade |
| ----- |
|       |
 -->
